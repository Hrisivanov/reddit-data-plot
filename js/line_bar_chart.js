d3.json('data.json', function(error, data) {
	data.data.forEach(function(d){
		d['sort'] = new Date(d.date)
	});
    data.data.sort(function(a, b) {
        return a.sort - b.sort;
    });

	let margin = {top: 50, right: 20, bottom: 30, left: 40},
      	width = 960,
      	height = 400;

    let xScale = d3.scaleBand()
                .rangeRound([0, width])
                .padding(0.1)
                .domain(data.data.map(function(d) {
                  return d.date;
                }));
        yScale = d3.scaleLinear()
                .rangeRound([height, 0])
                .domain([0, d3.max(data.data, (function (d) {
                  return d.total;
                }))]);

    let svg = d3.select("body").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom);

  	let g = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append('g')
    	.attr('class', 'grid')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    	.call(d3.axisLeft()
        	.scale(yScale)
        	.tickSize(-width, 0, 0)
        	.tickFormat(''))

    // axis-x
    g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xScale));

  	// axis-y
  	g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(yScale));

    var bar = g.selectAll("rect")
    .data(data.data)
    .enter().append("g");

  	// bar chart
  	bar.append("rect")
    	.attr("x", function(d) { return xScale(d.date); })
    	.attr("y", function(d) { return yScale(d.total); })
    	.attr("width", xScale.bandwidth())
    	.attr("height", function(d) { return height - yScale(d.total); })
    	.attr("class", "bar")
    	.on('mouseenter', function(actual, i){
    		d3.select(this)
          	  .transition()
          	  .duration(300)
              .attr('opacity', 0.6)
              .attr('x', (d) => xScale(d.date) - 5)
              .attr('width', xScale.bandwidth() + 10)

            let y = yScale(actual.total)
            line = svg.append('line')
          	  .attr('id', 'limit')
          	  .attr('x1', margin.left)
          	  .attr('y1', y + margin.bottom + 20)
          	  .attr('x2', width + margin.right + 20)
          	  .attr('y2', y + margin.bottom + 20)
    	})
    	.on('mouseleave', function(){
    		d3.select(this)
          	  .transition()
              .duration(300)
              .attr('opacity', 1)
              .attr('x', (d) => xScale(d.date))
              .attr('width', xScale.bandwidth())

            svg.selectAll('#limit').remove()
    	})

   	// labels on the bar chart
  	bar.append("text")
    	.attr("dy", "1.3em")
    	.attr("x", function(d) { return xScale(d.date) + xScale.bandwidth() / 2; })
    	.attr("y", function(d) { return yScale(d.total); })
    	.attr("text-anchor", "middle")
    	.attr("font-family", "sans-serif")
    	.attr("font-size", "11px")
    	.attr("fill", "black")
    	.text(function(d) {
      		return d.total;
      	});


    let div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    // line chart
  	var line = d3.line()
      .x(function(d, i) { return xScale(d.date) + xScale.bandwidth() / 2; })
      .y(function(d) { return yScale(d.classic_total); })
      .curve(d3.curveMonotoneX);

    bar.append("path")
    	.attr("class", "line") // Assign a class for styling
    	.attr("d", line(data.data)); // 11. Calls the line generator

  	bar.append("circle") // Uses the enter().append() method
      	.attr("class", "dot") // Assign a class for styling
      	.attr("cx", function(d, i) { return xScale(d.date) + xScale.bandwidth() / 2; })
      	.attr("cy", function(d) { return yScale(d.classic_total); })
      	.attr("r", 5)
      	.on("mouseover", function(d) {
            div.transition()
            .duration(200)
            .style("opacity", .9);

            div.html(
              'Classic Posts: ' + d.classic_total)
            .style("left", (d3.event.pageX + 5) + "px")
            .style("top", (d3.event.pageY - 20) + "px");
        })
        .on("mouseout", function(d) {
            div.transition()
            .duration(200)
            .style("opacity", 0);
          });

    svg.append("g")
      	.append("text")
      	.attr("class", "title")
      	.attr("x", width/2)
      	.attr("y", 0 + (margin.top / 2))
      	.attr("text-anchor", "middle")
      	.text("Total vs classic posts on /r/wow");

    let yScale2 = d3.scaleLinear()
                .rangeRound([height, 0])
                .domain([0, d3.max(data.data, (function (d) {
                  return d.classicWoW_total;
                }))]);
    let svg2 = d3.select("body").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom);

    svg2.append("g")
        .append("text")
        .attr("class", "title")
        .attr("x", width/2)
        .attr("y", 0 + (margin.top / 2))
        .attr("text-anchor", "middle")
        .text("Posts on /r/ClassicWoW");

    let g2 = svg2.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg2.append('g')
      .attr('class', 'grid')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(d3.axisLeft()
          .scale(yScale2)
          .tickSize(-width, 0, 0)
          .tickFormat(''))

    // axis-x
    g2.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xScale));

    // axis-y
    g2.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(yScale2));

    let bar2 = g2.selectAll("rect")
    .data(data.data)
    .enter().append("g");

    // bar chart
    bar2.append("rect")
      .attr("x", function(d) { return xScale(d.date); })
      .attr("y", function(d) { return yScale2(d.classicWoW_total); })
      .attr("width", xScale.bandwidth())
      .attr("height", function(d) { return height - yScale2(d.classicWoW_total); })
      .attr("class", "bar")
      .on('mouseenter', function(actual, i){
        d3.select(this)
              .transition()
              .duration(300)
              .attr('opacity', 0.6)
              .attr('x', (d) => xScale(d.date) - 5)
              .attr('width', xScale.bandwidth() + 10)

            let y = yScale2(actual.classicWoW_total)
            line = svg2.append('line')
              .attr('id', 'limit')
              .attr('x1', margin.left)
              .attr('y1', y + margin.bottom + 20)
              .attr('x2', width + margin.right + 20)
              .attr('y2', y + margin.bottom + 20)
      })
      .on('mouseleave', function(){
        d3.select(this)
              .transition()
              .duration(300)
              .attr('opacity', 1)
              .attr('x', (d) => xScale(d.date))
              .attr('width', xScale.bandwidth())

            svg2.selectAll('#limit').remove()
      })

    // labels on the bar chart
    bar2.append("text")
      .attr("dy", "1.3em")
      .attr("x", function(d) { return xScale(d.date) + xScale.bandwidth() / 2; })
      .attr("y", function(d) { return yScale2(d.classicWoW_total); })
      .attr("text-anchor", "middle")
      .attr("font-family", "sans-serif")
      .attr("font-size", "11px")
      .attr("fill", "black")
      .text(function(d) {
          return d.classicWoW_total;
        });
})