import requests
from datetime import datetime as dt
import time
import json

current_epoch = int(time.time())
url = f'https://api.pushshift.io/reddit/submission/search/?subreddit=wow&filter=created_utc&after=1557619200&before={current_epoch}&limit=500'
response = requests.get(url)
all_responses = []
all_responses.append(response)

classicWoW_url = f'https://api.pushshift.io/reddit/submission/search/?subreddit=ClassicWoW&filter=created_utc&after=1557619200&before={current_epoch}&limit=500'
classicWoW_response = requests.get(classicWoW_url)
all_classicWoW_responses = []
all_classicWoW_responses.append(classicWoW_response)

classic_url = f'https://api.pushshift.io/reddit/submission/search/?subreddit=wow&filter=created_utc&after=1557619200&before={current_epoch}&limit=500&title=classic'
classic_response = requests.get(classic_url)
classic_data = classic_response.json()['data']


while response.json()['data']:
    epoch = response.json()['data'][-1]['created_utc']
    url = f'https://api.pushshift.io/reddit/submission/search/?subreddit=wow&filter=created_utc&after={epoch}&before={current_epoch}&limit=500'
    response = requests.get(url)
    all_responses.append(response)

while classicWoW_response.json()['data']:
    classic_epoch = classicWoW_response.json()['data'][-1]['created_utc']
    classic_url = f'https://api.pushshift.io/reddit/submission/search/?subreddit=ClassicWoW&filter=created_utc&after={classic_epoch}&before={current_epoch}&limit=500'
    classicWoW_response = requests.get(classic_url)
    all_classicWoW_responses.append(classicWoW_response)

data = {'data': []}
temp_list = []
classic_temp_list = []
temp2_list = []
classic_temp2_list = []

for response in all_responses:
    response_data = response.json()['data']

    for submission in response_data:
        date = dt.utcfromtimestamp(submission['created_utc']).strftime('%Y-%m-%d')
        submission['created_utc'] = date

    temp_list.append(response_data)

for response in all_classicWoW_responses:
    classic_response_data = response.json()['data']

    for submission in classic_response_data:
        date = dt.utcfromtimestamp(submission['created_utc']).strftime('%Y-%m-%d')
        submission['created_utc'] = date

    classic_temp_list.append(classic_response_data)

for list_entry in temp_list:
    for dict_entry in list_entry:
        temp2_list.append(dict_entry)

for list_entry in classic_temp_list:
    for dict_entry in list_entry:
        classic_temp2_list.append(dict_entry)

unique_dates = set(val for dic in temp2_list for val in dic.values())

for date in list(unique_dates):
    entry = {}
    total = sum(1 for d in temp2_list if d.get('created_utc') == date)
    entry['total'] = total
    entry['date'] = date
    data['data'].append(entry)

for date in list(unique_dates):
    classicWoW_total = sum(1 for d in classic_temp2_list if d.get('created_utc') == date)
    for entry in data['data']:
        if entry['date'] == date:
            entry['classicWoW_total'] = classicWoW_total

for submission in classic_data:
    date = dt.utcfromtimestamp(submission['created_utc']).strftime('%Y-%m-%d')
    submission['created_utc'] = date

for date in list(unique_dates):
    classic_total = sum(1 for d in classic_data if d.get('created_utc') == date)
    for entry in data['data']:
        if entry['date'] == date:
            entry['classic_total'] = classic_total

with open('data.json', 'w') as outfile:
    json.dump(data, outfile)
